## Aprendiendo a analizar código en Python

Para este ejercicio hay que documentarse un poco acerca de operaciones aritméticas en python.

Analiza el siguiente código y `OJO` sin `ejecutar el programa` coloca el resultado que corresponda de la comparación.

```python
suma_uno = 8 + 10
suma_dos = suma_uno + 10
valor_uno = 10
valor_dos = 5
suma_dos + valor_dos == 18 + valor_uno + valor_dos
#>>> En esta parte coloca el resultado de la comparación

resta_uno = 58 - 10
valor_uno = "40"
valor_dos = "8.0"
suma_uno = int(valor_uno) + float(valor_dos)
resta_dos = resta_uno - suma_uno
resta_dos == 0
#>>> En esta parte coloca el resultado de la comparación

multiplicacion = 10 * 5
division = multiplicacion / 5
valor_uno = 8
operacion = division - valor_uno
valor_dos = "World"
cadena = "Hello" * operacion +  valor_dos
cadena == "HelloHelloWorld"
#>>> En esta parte coloca el resultado de la comparación
```
